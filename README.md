ANewHopeee
==========

Project for Software Design course on Faculty of Electrical Engineering and Computing.

Web-App written in C#/ASP.NET for restauraunt and food ordering with complete Admin page for editing meals, orders and employees.

Contributors:
------------
* Fredi Šarić
* Jan Kelemen
* Matej Janjić
* Tin Trčak
* Kenneth Kostrešević
* Domagoj Latečki
* Filip Gulan
